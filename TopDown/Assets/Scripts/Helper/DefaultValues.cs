using UnityEngine;
using System.Collections;

/// <summary>
///     This is to store our default values, we probablu want
/// to look at this a little more and store it on a SO so we can
/// store multiple instances.
/// </summary>
public static class DefaultValues
{
    public const float OFFSET_X = 0f;
    public const float OFFSET_Y = 13f;
    public const float OFFSET_Z = -7f;

    public static Vector3 DefaultOffset = new Vector3(OFFSET_X, OFFSET_Y, OFFSET_Z);

    public const float CAMERA_MOVE_SPEED_NORMAL = 0.2f;
    public const float CAMERA_MOVE_SPEED_RUNNING = 0.4f;

    public const KeyCode CameraFollowKeyCode = KeyCode.F;
    public const KeyCode PlayerRunningKeyCode = KeyCode.LeftShift;
}