using UnityEngine;
using System.Collections;

/// <summary>
///     This is just to store our tag's 
/// to prevent naked strings in the project.
/// </summary>
public static class InfoHelper
{
    public const string PlayerTag = "Player";
    public const string CameraTag = "MainCamera";
    public const string TerrainTag = "Terrain";

    public const string ManagerTag = "Manager";
    public const string InputTag = "InputController";
}