using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour
{
    public float RotationSpeed = 450f;
    public float WalkSpeed = 5f;
    public float RunSpeed = 8f;

    public float GravityModifier = -4;

    private bool _running = false;

    public bool Running
    {
        get { return _running; }
        set { _running = value; }   //We want some validation here to determine if we can run or not.
    }

    private Vector3 _motion;

    public Vector3 MyMotion;
    public Vector3 MyInput;

    private Quaternion _targetRotation;
    private CharacterController _controller;
    private MouseRayInput _mouseRayInput;

    #region Mono
    void Awake()
    {
        _controller = GetComponent<CharacterController>();

        _mouseRayInput = GameObject.FindGameObjectWithTag(InfoHelper.InputTag).GetComponent<MouseRayInput>();
    }

    void FixedUpdate()
    {
        Vector3 input = new Vector3(Input.GetAxisRaw("Horizontal"), 0f, Input.GetAxisRaw("Vertical"));  //Input.

        MyInput = input;
        
        _targetRotation = Quaternion.LookRotation(_mouseRayInput.MouseHit - transform.position);    //Takes a direction and make the player look at it.

        transform.eulerAngles = Vector3.up *
                        Mathf.MoveTowardsAngle(transform.eulerAngles.y, _targetRotation.eulerAngles.y,
                            RotationSpeed * Time.deltaTime);
        

        _motion = input.normalized;      //We take the normalized vector to make sure we have a max lenght of one.
        MyMotion = _motion; //This is just to save the Motion before we apply gravity and multiply.
        _motion += Vector3.up * GravityModifier;

        //TODO: Implement back-pedalling penalties
        //What we want here it's to compare the motion vector against the mouse direction
        //and do a dot-product, if it`s positive we don't apply penalties and if it's
        //negative we apply, so we limit the back-pedalling mechanic.

        _motion *= (_running) ? RunSpeed : WalkSpeed;
        _motion *= Time.fixedDeltaTime;
        
        _controller.Move(_motion);

#if UNITY_EDITOR
        Debug.DrawRay(transform.position, input * 2, Color.green);
        //Debug.Log("Input Vector: " + input.normalized);
#endif

    }
    #endregion

    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, 5f);
    }
}