using UnityEngine;
using System.Collections;
using UnityEngine.Assertions.Must;

/// <summary>
///     So, I was thinking and we should have a CameraMode class, 
/// this way we can just inherit from it and have some base functionallity
/// on it, this way we just need to write new code for new functionallity 
/// and our delegate on CameraController.cs can just decide what to do based on what we
/// want to have on the camera.
/// </summary>

public enum CameraMode
{
    None = 0,
    FollowPlayer = 1,
    FollowMouse = 3
}

[DisallowMultipleComponent]
public class CameraController : MonoBehaviour
{
    //TODO: Clear this after debug.
    public GameObject DebuGameObject;

    public float DistanceAheadCamera = 5f;

    [Tooltip("This is the SmoothDamp time.")]
    public float CameraNormalSpeed = 0.2f;

    [Tooltip("This is the SmoothDamp time.")]
    public float CameraRunningSpeed = 0.4f;

    [Tooltip("The max speed on the SmoothDamp.")]
    public float CameraMaxSpeed = 30f;

    [Tooltip("If we want the camera to LookAt the player or not.")]
    public bool LookAtPlayer = false;

    [Tooltip("The distance on each axis from the player.")]
    public Vector3 CameraOffset = new Vector3(0f, 10f, -12f);

    /// <summary>
    ///     If we need to change the offset from outside this class.s
    ///     We need to implement some validation here so we don't have 
    /// stupid situations.
    /// </summary>
    public Vector3 CameraTargetOffset
    {
        get { return _cameraTargetWithOffset;}
        set { _cameraTargetWithOffset = value; }
    }

    /// <summary>
    ///     If we want to check the target from outside this class
    /// just to do whatever.
    /// </summary>
    public Vector3 OffSetCalculated
    {
        get { return new Vector3(_targetX, _targetY, _targetZ); }
    }

    private CameraMode _cameraMode = CameraMode.FollowPlayer;
    
    /// <summary>
    ///     To change the camera we want to call this property.
    ///     This will determine, based on the value what our
    /// delegate will be running.
    ///     It's a switch-case to allow for easy post implementation.
    /// </summary>
    public CameraMode MyCameraMode
    {
        get { return _cameraMode; }
        set
        {
            _cameraMode = value;
            _cameraFollow = null;   //We reset the delegate.

            switch (value)
            {
                case CameraMode.FollowMouse:
                    _cameraFollow += FollowMouse;
                    break;

                case CameraMode.FollowPlayer:
                    _cameraFollow += FollowPlayer;
                    break;
            }

            ////We add the right method to the delegate.
            //if (_cameraMode == CameraMode.FollowPlayer)
            //    _cameraFollow += FollowPlayer;      
            //else if (_cameraMode == CameraMode.FollowMouse)
            //    _cameraFollow += FollowMouse;
        }
    }
    
    private Vector3 _cameraTarget;
    private Vector3 _cameraTargetWithOffset;

    private float _targetX = 0f;
    private float _targetY = 0f;
    private float _targetZ = 0f;

    private Vector3 _refVelocity = Vector3.zero;

    private float _cameraSpeed;

    private Transform _playerTransform;
    private PlayerController _playerController;
    private MouseRayInput _mouseRayInput;

    private delegate void CameraFollow();
    private CameraFollow _cameraFollow;

    #region Mono

    void Awake()
    {

        _playerTransform = GameObject.FindGameObjectWithTag(InfoHelper.PlayerTag).GetComponent<Transform>();
        _playerController = GameObject.FindGameObjectWithTag(InfoHelper.PlayerTag).GetComponent<PlayerController>();
        _mouseRayInput = GameObject.FindGameObjectWithTag(InfoHelper.InputTag).GetComponent<MouseRayInput>();
    }

    void Start()
    {
        _cameraTarget = new Vector3(_playerTransform.position.x, transform.position.y, _playerTransform.position.z);
        
        _targetX = _playerTransform.position.x + CameraOffset.x;
        _targetY = _playerTransform.position.y + CameraOffset.y;
        _targetZ = _playerTransform.position.z + CameraOffset.z;

        _cameraTargetWithOffset = new Vector3(_targetX, _targetY, _targetZ);
        
        transform.position = Vector3.Lerp(transform.position, _cameraTarget, Time.deltaTime*8);

        MyCameraMode = CameraMode.FollowPlayer; //The default will be to follow the player.

        ControlCameraSpeed();
    }
    
    /// <summary>
    ///     If our delegate isn't null we run the 
    /// main method of our CameraController.
    ///     We check for the speed to change it if we
    /// have changed the enum from outside this class,
    /// (TODO)there is any way I don't need to check this every frame?
    /// Like should trigger a event or something to tell it to change
    /// the speed?
    /// </summary>
    void LateUpdate()
    {
        if (_cameraFollow != null)
        {
            ControlCameraSpeed();
            _cameraFollow();
        }
    }


    #endregion

    private void ControlCameraSpeed()
    {
        if (_playerController.Running)
            _cameraSpeed = CameraRunningSpeed;
        else
            _cameraSpeed = CameraNormalSpeed;

    }

    private void FollowPlayer()
    {

        //if (_playerController.Running)
        //{
        //    Vector3 localMotion = _playerController.MyMotion;

        //    if (localMotion != Vector3.zero)
        //    {
        //        localMotion *= DistanceAheadCamera;
        //        localMotion += _playerTransform.position;

        //        _targetX = localMotion.x;
        //        _targetY = _playerTransform.position.y + CameraOffset.y;
        //        _targetZ = localMotion.z;
        //    }

        //    //_targetX = Mathf.Lerp(_targetX, localMotion.x, Time.deltaTime);
        //    //_targetY = Mathf.Lerp(_targetY, localMotion.y, Time.deltaTime);
        //    //_targetZ = Mathf.Lerp(_targetZ, localMotion.z, Time.deltaTime);

        //    DebuGameObject.transform.position = new Vector3(_targetX, localMotion.y, _targetZ);

        //    Debug.DrawLine(_playerTransform.position, new Vector3(_targetX, _targetY, _targetZ), Color.red);
        //}
        //else if (!_playerController.Running)
        //{
        //    _targetX = Mathf.Lerp(_targetX, (_playerTransform.position.x + CameraOffset.x), Time.deltaTime);
        //    _targetY = Mathf.Lerp(_targetY, (_playerTransform.position.y + CameraOffset.y), Time.deltaTime);
        //    _targetZ = Mathf.Lerp(_targetZ, (_playerTransform.position.z + CameraOffset.z), Time.deltaTime);       
        //}

        _targetX = Mathf.Lerp(_targetX, (_playerTransform.position.x + CameraOffset.x), Time.deltaTime);
        _targetY = Mathf.Lerp(_targetY, (_playerTransform.position.y + CameraOffset.y), Time.deltaTime);
        _targetZ = Mathf.Lerp(_targetZ, (_playerTransform.position.z + CameraOffset.z), Time.deltaTime);

        _cameraTargetWithOffset = new Vector3(_targetX, _targetY, _targetZ);

        transform.position = Vector3.SmoothDamp(transform.position, _cameraTargetWithOffset, ref _refVelocity,
            _cameraSpeed, CameraMaxSpeed);

        //Debug.DrawRay(transform.position, _cameraTarget, Color.green);        

        if (LookAtPlayer)
            transform.LookAt(_playerTransform.position);
    }

    /// <summary>
    ///     This will make the camera follow the mouse.
    ///     If our mouseTarget it's equal to zero, we will make
    /// the focus be the player position, this is to avoid going to the 0,0,0
    /// on the terrain if we are not focusing it with the mouse.
    ///     TODO:We can expand it to be some mechanic in the game.
    /// </summary>
    private void FollowMouse()
    {
        Vector3 mouseTarget = _mouseRayInput.MouseHitWithOffset;
        
        if (mouseTarget != Vector3.zero)
        {
            _targetX = Mathf.Lerp(_targetX, (mouseTarget.x + CameraOffset.x), Time.deltaTime);
            _targetY = Mathf.Lerp(_targetY, (mouseTarget.y + CameraOffset.y + _playerTransform.position.y),
                Time.deltaTime);
            _targetZ = Mathf.Lerp(_targetZ, (mouseTarget.z + CameraOffset.z), Time.deltaTime);
        }
        else
        {
            _targetX = Mathf.Lerp(_targetX, (_playerTransform.position.x + CameraOffset.x), Time.deltaTime);
            _targetY = Mathf.Lerp(_targetY, (_playerTransform.position.y + CameraOffset.y), Time.deltaTime);
            _targetZ = Mathf.Lerp(_targetZ, (_playerTransform.position.z + CameraOffset.z), Time.deltaTime);
        }
        
        _cameraTargetWithOffset = new Vector3(_targetX, _targetY, _targetZ);

        transform.position = Vector3.SmoothDamp(transform.position, _cameraTargetWithOffset, ref _refVelocity,
            _cameraSpeed, CameraMaxSpeed);
    }
}