using UnityEngine;
using System.Collections;

public class MouseRayInput : MonoBehaviour
{
    //TODO: Check if we can use the distance from the camera plus something.
    [Tooltip("The lenght we will fire our ray. This probably can be the distance from the camera?")]
    public float RayLength = 20f;

    [Tooltip("The max distance the Vector Target will be from the Player, axis X and Z.")]
    public Vector2 MaxDistanceFromPlayerXZ = new Vector2(5, 5);

    private float _targetX;
    private float _targetZ;
    
    private Transform _playerTransform;

    private int TerrainLayerMask = 1 << 8;

    void Awake()
    {
        _playerTransform = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
    }

    //This is really confusing. Why do I need this ?
    public Vector3 MouseHitWithOffset { get { return GetMouseRayHitWithOffset(); } }

    private Vector3 GetMouseRayHitWithOffset()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit rayHit;

        Vector3 target = new Vector3();

        ray.direction = ray.direction*RayLength;

        if (Physics.Raycast(ray, out rayHit, RayLength, TerrainLayerMask))
        {
            if (rayHit.collider.gameObject.CompareTag("Terrain"))
            {
                _targetX = Mathf.Clamp(rayHit.point.x, _playerTransform.position.x - MaxDistanceFromPlayerXZ.x,
                    _playerTransform.position.x + MaxDistanceFromPlayerXZ.x);

                _targetZ = Mathf.Clamp(rayHit.point.z, _playerTransform.position.z - MaxDistanceFromPlayerXZ.y,
                    _playerTransform.position.z + MaxDistanceFromPlayerXZ.y);

                target = new Vector3(_targetX, 0, _targetZ);
            }
        }

#if UNITY_EDITOR

        Debug.DrawLine(ray.origin, rayHit.point, Color.red);

#endif

        return target;
    }

    //This is really confusing. Why do I need this ?
    public Vector3 MouseHit { get { return GetMouseRayHit(); } }

    private Vector3 GetMouseRayHit()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit rayHit;

        Vector3 target = new Vector2();

        ray.direction = ray.direction*RayLength;

        if (Physics.Raycast(ray, out rayHit, RayLength, TerrainLayerMask))
        {
            target = new Vector3(rayHit.point.x, 0f, rayHit.point.z);
        }

        //Debug.DrawLine(ray.origin, rayHit.point, Color.red);
        //Debug.Log(rayHit.point + " / " + target);
        
        return target;
    }
}