using UnityEngine;
using System.Collections;

//This name is bad, we should move this code to another class and make this one only controller input.
public class InputController : MonoBehaviour
{
    #region Singleton
    public static InputController Instance { get; private set; }

    private void SingletonSetup()
    {
        if (Instance != this && Instance != null)
        {
            Destroy(this);
        }
        else
        {
            Instance = this;
        }
    }
    #endregion

    //TODO: Move this to DefaulValues and have some way to allow customization on controllers.
    public KeyCode CameraFollowKeyCode = KeyCode.F;
    public KeyCode PlayerRunningKeyCode = KeyCode.LeftShift;

    private PlayerController _playerController;
    private CameraController _cameraController;

    void Awake()
    {
        SingletonSetup();

        _playerController = GameObject.FindGameObjectWithTag(InfoHelper.PlayerTag).GetComponent<PlayerController>();
        _cameraController = GameObject.FindGameObjectWithTag(InfoHelper.CameraTag).GetComponent<CameraController>();
    }

    void Update()
    {
        if (Input.GetKeyUp(CameraFollowKeyCode))
        {
            if (_cameraController.MyCameraMode != CameraMode.FollowPlayer)
                _cameraController.MyCameraMode = CameraMode.FollowPlayer;
        }

        if (Input.GetKeyDown(CameraFollowKeyCode))
        {
            if (_cameraController.MyCameraMode != CameraMode.FollowMouse)
            {
                if (!_playerController.Running)
                    _cameraController.MyCameraMode = CameraMode.FollowMouse;
            }
        }

        if (Input.GetKeyUp(PlayerRunningKeyCode))
        {
            if (_playerController.Running)
                _playerController.Running = false;
        }

        if (Input.GetKeyDown(PlayerRunningKeyCode))
        {
            if (!_playerController.Running)
                _playerController.Running = true;
        }

    }
    
}